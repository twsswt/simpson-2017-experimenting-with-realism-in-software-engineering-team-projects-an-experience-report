Citation. 

Simpson, R. and Storer, T. (2017) Experimenting with Realism in Software Engineering Team Projects: An Experience Report. In: 
30th IEEE Conference on Software Engineering Education and Training (CSEE&T), Savannah, GA, USA, 7-9 Nov 2017.

22 full papers of 77 accepted (28%).
