import matplotlib.pyplot as plt

# Data to plot

data = [
        ['Requirements gathering', 60, '#00EE99'],
        ['Progress demonstrations', 40, '#00EE88'],
        ['Change management', 25, '#00EE77'],
        ['QA', 25, '#00EE66'],
        ['planning', 25, '#00EE55'],
        ['process improvement', 25, '#00EE44'],
        ['Hand over', 40, '#00EE33'],
        ['Design and implementation quality ', 40, '#EE5555'],
        ['Ambition', 20, '#EE4444'],
        ['Reflective case study dissertation', 100, '#9999FF'],
]

labels = map(lambda t: t[0], data)
sizes = map(lambda t: t[1], data)
colors = map(lambda t: t[2], data)


explode = [0.1].append([0.0] * (len(data) - 1))   # explode 1st slice



#plt.rc('font', **{'family':'serif', 'serif':['Georgia'], 'size': 10})

#plt.figure(figsize=(2.5, 2.5/1.4))

# Plot
plt.pie(sizes, explode=explode, labels=labels, colors=colors,
        autopct='%1.1f%%', shadow=True, startangle=140)

plt.axis('equal')

plt.savefig('new-assessment.jpg', bbox_inches='tight')