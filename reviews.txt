----------------------- REVIEW 1 ---------------------
PAPER: 52
TITLE: Experimenting with Realism in Software Engineering Team Projects:
An Experience Report
AUTHORS: Robbie Simpson and Tim Storer
 
 
----------- Overall evaluation -----------
This paper reports on experience gained by changes gradually applied
since 2014 in the delivery of software engineering topics within the
Computing Science Curriculum at the University of Glasgow.
 
After an overview of problems perceived during the two existing courses
on software engineering (a traditional lecture on Professional Software
Development and a Software Engineering Team Project course), the paper
describes the different changes (including the underlying reasons)
applied to the teaching strategy (e.g. focus on agile processes,
flipped-classroom, real-world project customers, educating and involving
final year students as student mentors).
 
A first evaluation of the new teaching strategy was carried out using
semi-structured interviews aiming at getting feedback from different
viewpoints. As a main part of the paper, the results of these interviews
are discussed for each category of participants (e.g. team project
students, student mentors, customers).
 
The findings presented are quite interesting and conceivable. The rather
small sample size (3 team project students, 5 customers, 5 student
mentors) allowed for presenting the feedback by anonymized individuals.
However, this is also the main drawback of the paper, which includes
rather long-winded sections of subjective viewpoints of only few
individuals.
 
Nevertheless, the few interview sessions certainly unfolded a whole
bunch of items for a further improvement of the courses (e.g. to attach
mentors to certain student teams, educating customer representatives in
their roles, formal project delivery).
 
As a next step, I would suggest applying these improvement possibilities
and formulating these as questions during the next course evaluation,
preferably including questionnaires involving all participants for their
feedback, which can be used in a quantitative way of analysis.
 
Just for clarification:
Which agile process model(s) was/were used by the teams? Who selected
the actual kind of process (the instructor, the team, the customer)?
 
Some typos:
pg. 4, col. 2, last par.: ��The mentors then designed and implementED ����
pg. 5, col. 2, line 6: ���� and did work together ��
 
 
----------------------- REVIEW 2 ---------------------
PAPER: 52
TITLE: Experimenting with Realism in Software Engineering Team Projects:
An Experience Report
AUTHORS: Robbie Simpson and Tim Storer
 
 
----------- Overall evaluation -----------
The paper describes revisions made to student group projects in software
engineering in order to provide both a more realistic experience for
students and also to persuade students of its relevance.  The major
contribution of this paper lies not so much in the originality of these,
as in the rigorous way that they were explained and evaluated.
 
This paper is well written (bar a few grammatical slips and typos,
listed below), explaining the issues clearly and addressing a topic that
many of us find difficult to address as successfully as we would like.
The use of of semi-structured interviews provides a sound empirical
basis for the evaluation of their experiences (although arguably, it
would be useful to explain why this was the preferred approach, this
aspect is rather assumed).  I particularly liked the reference to
ethical approval, which easily gets overlooked.  The latter part of the
paper provides a useful and interesting qualitative analysis of their
experiences, based upon the views of the different types of participant,
and there is also a very useful section that compares the experiences of
the authors with those provided in previous studies.
 
The paper does perhaps have a rather strong UK focus (the influence of
the BCS accreditation scheme is acknowledged in the opening paragraph),
although that does not necessarily suggest that the experiences do not
translate into other contexts. For those unfamiliar with the Scottish
flavour of UK higher education, it might be useful to provide a brief
discussion of the way that four-year degrees are organised.
 
Having been involved with group projects over the years, I found it hard
not to empathise with the experiences and views expressed in this paper!
  It provides a useful set of experiences that others can learn from and
draws well-balanced conclusions.
 
Typos etc.
 
Abstract: line 5: 'impose' should be 'imposed'
Section I, page 2, column 2, para 2, line 7: 'Bull and Whittle [3]
notes' should be 'Bull and Whittle [3] note' (it is the multiple authors
not the singular paper that are being quoted here).
Section IIA, page 2, line 2: 'introduced a' should be 'introduced to a'
Section iIB, page 3, para 3, penultimate line: an instance of "it's"
that should be "its"
Page 5, first line: "students planning" should be "students' planning"
Section III, page 5, first para, line 14: 'for inspection' should be
'for inspection in'
Page 5, column 2, last line -2: "the teams they were allocated" is
correct but not very easy to scan.  Why not say "that were allocated to
them".
 
 
----------------------- REVIEW 3 ---------------------
PAPER: 52
TITLE: Experimenting with Realism in Software Engineering Team Projects:
An Experience Report
AUTHORS: Robbie Simpson and Tim Storer
 
 
----------- Overall evaluation -----------
This paper describes, in detail, the experience of restructuring a
software team project course such that students get to do more realistic
project while working with real customers. It appears the restructuring
involved a significant effort, linking up multiple stakeholders/courses,
even creating new courses to scaffold the course in concern. Kudos to
the authors for effort.
While the various parts of the restructuring are not entirely novel, I
think the paper has value to the CSEET community at least as a
well-documented positive data point making the case for providing more
realistic contexts in SE courses.
Some questions authors can address:
* Was the considerable effort worth it in the end? Quite often this kind
of revamps are quite satisfying to ��pull off�� but the people involved
run out of steam soon after and the sustainability becomes a problem.
* Was there a significant improvement in terms of learning outcomes, in
reality, and as perceived by various parties involved?
Oher areas to improve: Use more data points? Interviewing only 3
students from the course seem too few?
Minor typo: outwith
