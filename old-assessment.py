import matplotlib.pyplot as plt

# Data to plot

data = [
        ['Final presentation', 10, '#00EE88'],
        ['Requirements documentation', 20, '#EE5555'],
        ['Product Design and implementation ', 40, '#EE4444'],
        ['Evaluation report', 20, '#EE3333'],
        ['Dissertation', 10, '#9999FF'],
]

labels = map(lambda t: t[0], data)
sizes = map(lambda t: t[1], data)
colors = map(lambda t: t[2], data)


explode = [0.1].append([0.0] * (len(data) - 1))   # explode 1st slice

# Plot
plt.pie(sizes, explode=explode, labels=labels, colors=colors,
        autopct='%1.1f%%', shadow=True, startangle=140)

plt.axis('equal')

plt.savefig('old-assessment.jpg', bbox_inches='tight')